# Programming Assignment 2

0510010 陳翰群



# Decision Tree Implementation

## Node Data Sturcture

MyDecisionTreeClassifier.py 包含了能獨立運作的class：MyDecisionTreeClassifier，以及測試決策樹的函數test_tree。

由於使用CART二元樹，在MyDecisionTreeClassifier內部定義class DecisionNode，其中每個Node都有true branch和false branch來作為向下傳遞的分支，summary記錄進行到該Node的Gini不純度或是熵，連同判斷式，提供後續視覺化所需的資訊。

DecisionNode成員如下：

```python
class DecisionNode:
  	def __init__(self, col=-1, value=None, results=None, tb=None, 												fb=None, dc_headings=None, summary=None):
				self.col = col
				self.value = value
				self.results = results  # None for nodes, not None for leaves
   			self.tb = tb	# True branch
   		  self.fb = fb	# False branch
   		  self.dc_headings = dc_headings	# Name of the features
   		  self.summary = summary
```

## Inducton：從Training Data建立決策樹

為了方便處理資料，所有Dataset都要轉換成CSV格式，然後透過CSVReader來轉換成2D的List。

CSVReader.py定義了多個針對Dataset的函數，例如read_cross200()、read_ellipse100()、read_iris()...等等。以上函數都回return：header和list並送入決策樹訓練。

###MyDecisionTreeClassifier

除了DecisionNode之外，Classifier內部有數個成員與方法（省略部分）：

```python
class MyDecisionTreeClassifier:
  	def __init__(self, max_depth=-1, 
                 random_features=True, 
                 dc_header=None):
      	self.root_node = None
        self.max_depth = max_depth
        self.features_indexes = []
        self.random_features = random_features
        self.dc_header = dc_header
    def fit(self, rows, criterion="entropy")
    def predict(self, features)
    def entropy(self, rows)
    def gini(self, rows)
    def build_tree(self, rows, func, depth)
```

 剛建立Classifier時可以決定是否要隨機選擇features，另外在進行fit()時可以選擇使用entropy或是gini來計算。

### build_tree

1. Select a threshold for a given attribute：內部使用for迴圈來將各種features遍歷一遍，內層迴圈將所有數值嘗試一遍，並且使用entropy()或gini()來計算current_score。

2. Divide sample based on best Gini's impurity gain：取值gain最高的作為分割Node的依據，同時更新summary，若是最佳的gain>0，就依序對true_branch和false_branch傳入新的build_tree()。

3. 最後build_tree向上傳遞DecisionNode實體，將整個tree從Leaf往Root生成出完整的Tree。

詳細請參照MyDecisionTreeClassifier.py，以下是計算Gini's不純度的函數

```python
class MyDecisionTreeClassifier:
    def gini(self, rows):
        total = len(rows)
        counts = self.unique_counts(rows)
        impurity = 0.0

        for k1 in counts:
            p1 = float(counts[k1]) / total
            for k2 in counts:
                if k1 == k2:
                    continue
                p2 = float(counts[k2]) / total
                impurity += p1 * p2
        return impurity
```

## Inference：分類新的資料

使用MyDecisionTreeClassifier : classify()方法：

```python
class MyDecisionTreeClassifier:
    def classify(self, observation, tree):
        if tree.results is not None:
            return list(tree.results.keys())[0]
        else:
            v = observation[tree.col]
            if isinstance(v, int) or isinstance(v, float):
                if v >= tree.value:
                    branch = tree.tb
                else:
                    branch = tree.fb
            else:
                if v == tree.value:
                    branch = tree.tb
                else:
                    branch = tree.fb
            return self.classify(observation, branch)
```

兩個參數：

observation：想要進行分類的Sample所有的features數值

tree：傳入DecisionNode實體

每次檢查是否已經到達決策樹的底部，如果沒有就根據記錄的value來選擇走true_branch或false_branch，然後遞迴帶入下一層的classify()。

最終會回傳字串是分類的結果。

##Attribute Bagging 

在呼叫fit()函數時可以選擇是否進行Attribute bagging，如果尚有有n個features，則會隨機選擇sqrt(n)個features來逐一計算最大的gain：

```python
def fit(self, rows, criterion="gini"):
		......
		if self.random_features:
				self.features_indexes = self.choose_random_features(rows[0])
				rows = [self.get_features_subset(row) + 
                [row[-1]] for row in rows]
@staticmethod
def choose_random_features(row):
		nb_features = len(row) - 1
		return random.sample(range(nb_features), int(sqrt(nb_features)))
```

### Bagging 效果

使用針對iris dataset 建構三次決策樹，結果視覺化如下

![image-20190430213156945](assets/image-20190430213156945.png)

![image-20190430213213067](assets/image-20190430213213067.png)

![image-20190430213233090](assets/image-20190430213233090.png)

可以看到每次建構出來的樹都長得不一樣



# Random Forest Implementation

定義於MyRandomForestClassifier.py。有class MyRandomForestClassifier：

```python
class MyRandomForestClassifier(object):
  	    def __init__(self, nb_trees, nb_samples, 
                     max_depth=-1, max_workers=1):
        self.trees = []
        self.nb_trees = nb_trees
        self.nb_samples = nb_samples
        self.max_depth = max_depth
        self.max_workers = max_workers
```

在初始化classifier時可以指定：需要的tree數量、每個tree傳入的sample數量、限制tree的深度、以及同時間最大執行緒數量。

## Tree Bagging

由於能夠指定每個tree傳入的sample數量，使用random.sample來隨機從訓練集選擇丟入個別決策樹的sample數量：

```python
def fit(self, data):
  	......
		rand_fts = map(lambda x: [x, random.sample(data, self.nb_samples)],
                           range(self.nb_trees))
		self.trees = list(executor.map(self.train_tree, rand_fts))
```

nb_samples必須小於data的數量。詳細請參照MyRandomForestClassifier.py。

## Training vs Validation subset

在此使用來自sklearn.model_selection 的train_test_split() 將25%的data分出來用作validation subset，以便計算正確率。



# 實驗結果

以下除非特別說明，一律都是在100個Tree下運行的結果

## Ellipse100

75 Training Data，Tree Bagging取出60個（80%），準確度：76%

![image-20190430220245242](assets/image-20190430220245242.png)

## Cross200

150 Training Data，Tree Bagging取出120個（80%），準確度：78%

![image-20190430220406262](assets/image-20190430220406262.png)

## Iris Data Set

113 Training Data，Tree Bagging取出90個（80%），準確度：94.73%

![image-20190430220526523](assets/image-20190430220526523.png)

改用1000個tree，準確度：97.37%

![image-20190430220700868](assets/image-20190430220700868.png)

##  Optical Recognition of Handwritten Digits

2867 Training Data，Tree Bagging取出2294個（80%），準確度：96.34%

![image-20190430222315054](assets/image-20190430222315054.png)

## **Wine Quality Data Set** - White Data Set

4898 Samples，11 attributes，7 classes

3674 Training Data，Tree Bagging取出2939個（80%），準確度：64.97%

![image-20190430230233001](assets/image-20190430230233001.png)

## **Glass Identification Data Set** 

160 Training Data，Tree Bagging取出128個（80%），準確度：94.44%

![image-20190430231616527](assets/image-20190430231616527.png)

## **Ionosphere Data Set** 

263 Training Data，Tree Bagging取出210個（80%），準確度：93.18%

![image-20190430231941939](assets/image-20190430231941939.png)



# Codes

簡易到