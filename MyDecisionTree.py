import csv
from collections import defaultdict

import pydotplus


class DecisionTree:
    """Binary tree implementation with true and false branch. """

    def __init__(self, col=-1, value=None, true_branch=None, false_branch=None, results=None, summary=None):
        self.col = col
        self.value = value
        self.true_branch = true_branch
        self.false_branch = false_branch
        self.results = results  # None for nodes, not None for leaves
        self.summary = summary


def divide_set(rows, column, value):
    if isinstance(value, int) or isinstance(value, float):  # for int and float values
        splitting_function = lambda row: row[column] >= value
    else:  # for strings
        splitting_function = lambda row: row[column] == value
    list1 = [row for row in rows if splitting_function(row)]
    list2 = [row for row in rows if not splitting_function(row)]
    return list1, list2


def unique_counts(rows):
    results = {}
    for row in rows:
        # response variable is in the last column
        r = row[-1]
        if r not in results:
            results[r] = 0
        results[r] += 1
    return results


def entropy(rows):
    from math import log
    log2 = lambda x: log(x) / log(2)
    results = unique_counts(rows)

    entr = 0.0
    for r in results:
        p = float(results[r]) / len(rows)
        entr -= p * log2(p)
    return entr


def gini(rows):
    total = len(rows)
    counts = unique_counts(rows)
    imp = 0.0

    for k1 in counts:
        p1 = float(counts[k1]) / total
        for k2 in counts:
            if k1 == k2:
                continue
            p2 = float(counts[k2]) / total
            imp += p1 * p2
    return imp


def variance(rows):
    if len(rows) == 0:
        return 0
    data = [float(row[len(row) - 1]) for row in rows]
    mean = sum(data) / len(data)

    var = sum([(d - mean) ** 2 for d in data]) / len(data)
    return var


def grow_decision_tree_from(rows, evaluation_function=entropy):
    """Grows and then returns a binary decision tree.
    evaluation_function: entropy or gini"""

    if len(rows) == 0:
        return DecisionTree()
    current_score = evaluation_function(rows)

    best_gain = 0.0
    best_attribute = None
    best_sets = None

    column_count = len(rows[0]) - 1  # last column is the result/target column
    for col in range(0, column_count):
        column_values = [row[col] for row in rows]

        # unique values
        ls_unique = list(set(column_values))

        for value in ls_unique:
            (set1, set2) = divide_set(rows, col, value)

            # Gain -- Entropy or Gini
            p = float(len(set1)) / len(rows)
            gain = current_score - p * evaluation_function(set1) - (1 - p) * evaluation_function(set2)
            if gain > best_gain and len(set1) > 0 and len(set2) > 0:
                best_gain = gain
                best_attribute = (col, value)
                best_sets = (set1, set2)

    dc_y = {'impurity': '%.3f' % current_score, 'samples': '%d' % len(rows)}
    if best_gain > 0:
        true_branch = grow_decision_tree_from(best_sets[0], evaluation_function)
        false_branch = grow_decision_tree_from(best_sets[1], evaluation_function)
        return DecisionTree(col=best_attribute[0], value=best_attribute[1], true_branch=true_branch,
                            false_branch=false_branch, summary=dc_y)
    else:
        return DecisionTree(results=unique_counts(rows), summary=dc_y)


def prune(tree, min_gain, evaluation_function=entropy, notify=False):
    """Prunes the obtained tree according to the minimal gain (entropy or Gini). """
    # recursive call for each branch
    if tree.true_branch.results is None:
        prune(tree.true_branch, min_gain, evaluation_function, notify)
    if tree.false_branch.results is None:
        prune(tree.false_branch, min_gain, evaluation_function, notify)

    # merge leaves (potentionally)
    if tree.true_branch.results is not None and tree.false_branch.results is not None:
        tb, fb = [], []

        for v, c in tree.true_branch.results.items():
            tb += [[v]] * c
        for v, c in tree.false_branch.results.items():
            fb += [[v]] * c

        p = float(len(tb)) / len(tb + fb)
        delta = evaluation_function(tb + fb) - p * evaluation_function(tb) - (1 - p) * evaluation_function(fb)
        if delta < min_gain:
            if notify:
                print('A branch was pruned: gain = %f' % delta)
            tree.true_branch, tree.false_branch = None, None
            tree.results = unique_counts(tb + fb)


def classify(observations, tree, data_missing=False):
    """Classifies the observations according to the tree.
    data_missing: true or false if data are missing or not. """

    def classify_without_missing_data(_observations, _tree):
        if _tree.results is not None:  # leaf
            return _tree.results
        else:
            v = _observations[_tree.col]
            if isinstance(v, int) or isinstance(v, float):
                if v >= _tree.value:
                    branch = _tree.true_branch
                else:
                    branch = _tree.false_branch
            else:
                if v == _tree.value:
                    branch = _tree.true_branch
                else:
                    branch = _tree.false_branch
        return classify_without_missing_data(_observations, branch)

    def classify_with_missing_data(_observations, _tree):
        if _tree.results is not None:  # leaf
            return _tree.results
        else:
            v = _observations[_tree.col]
            if v is None:
                tr = classify_with_missing_data(_observations, _tree.true_branch)
                fr = classify_with_missing_data(_observations, _tree.false_branch)
                tcount = sum(tr.values())
                fcount = sum(fr.values())
                tw = float(tcount) / (tcount + fcount)
                fw = float(fcount) / (tcount + fcount)
                _result = defaultdict(int)
                for k, v in tr.items():
                    _result[k] += v * tw
                for k, v in fr.items():
                    _result[k] += v * fw
                return dict(_result)
            else:
                if isinstance(v, int) or isinstance(v, float):
                    if v >= _tree.value:
                        branch = _tree.true_branch
                    else:
                        branch = _tree.false_branch
                else:
                    if v == _tree.value:
                        branch = _tree.true_branch
                    else:
                        branch = _tree.false_branch
            return classify_with_missing_data(_observations, branch)

    # function body
    if data_missing:
        return classify_with_missing_data(observations, tree)
    else:
        return classify_without_missing_data(observations, tree)


def plot(decision_tree):
    """Plots the obtained decision tree. """

    def to_string(dc_tree, indent=''):
        if dc_tree.results is not None:  # leaf node
            ls_x = [(x, y) for x, y in dc_tree.results.items()]
            ls_x.sort()
            sz_y = ', '.join(['%s: %s' % (x, y) for x, y in ls_x])
            return sz_y
        else:
            sz_col = 'Column %s' % dc_tree.col
            if sz_col in dcHeadings:
                sz_col = dcHeadings[sz_col]
            if isinstance(dc_tree.value, int) or isinstance(dc_tree.value, float):
                decision = '%s >= %s?' % (sz_col, dc_tree.value)
            else:
                decision = '%s == %s?' % (sz_col, dc_tree.value)
            true_branch = indent + 'yes -> ' + to_string(dc_tree.true_branch, indent + '\t\t')
            false_branch = indent + 'no  -> ' + to_string(dc_tree.false_branch, indent + '\t\t')
            return decision + '\n' + true_branch + '\n' + false_branch

    print(to_string(decision_tree))


def dotgraph(decision_tree):
    global dcHeadings
    dc_nodes = defaultdict(list)
    """Plots the obtained decision tree. """

    def to_string(_i_split, _decision_tree, _b_branch, _sz_parent="null", indent=""):
        if _decision_tree.results is not None:  # leaf node
            ls_x = [(x, y) for x, y in _decision_tree.results.items()]
            ls_x.sort()
            sz_y = ', '.join(['%s: %s' % (x, y) for x, y in ls_x])
            dc_y = {"name": sz_y, "parent": _sz_parent}
            dc_summary = _decision_tree.summary
            dc_nodes[_i_split].append(['leaf', dc_y['name'], _sz_parent, _b_branch, dc_summary['impurity'],
                                       dc_summary['samples']])
            return dc_y
        else:
            sz_col = 'Column %s' % _decision_tree.col
            if sz_col in dcHeadings:
                sz_col = dcHeadings[sz_col]
            if isinstance(_decision_tree.value, int) or isinstance(_decision_tree.value, float):
                _decision = '%s >= %s' % (sz_col, _decision_tree.value)
            else:
                _decision = '%s == %s' % (sz_col, _decision_tree.value)
            trueBranch = to_string(_i_split + 1, _decision_tree.true_branch, True, _decision, indent + '\t\t')
            falseBranch = to_string(_i_split + 1, _decision_tree.false_branch, False, _decision, indent + '\t\t')
            dc_summary = _decision_tree.summary
            dc_nodes[_i_split].append([_i_split + 1, _decision, _sz_parent, _b_branch, dc_summary['impurity'],
                                       dc_summary['samples']])
            return

    to_string(0, decision_tree, None)
    ls_dot = ['digraph Tree {',
              'node [shape=box, style="filled, rounded", color="black", fontname=helvetica] ;',
              'edge [fontname=helvetica] ;'
              ]
    i_node = 0
    dc_parent = {}
    for nSplit in range(len(dc_nodes)):
        ls_y = dc_nodes[nSplit]
        for lsX in ls_y:
            i_split, decision, sz_parent, b_branch, sz_impurity, sz_samples = lsX
            if type(i_split) == int:
                sz_split = '%d-%s' % (i_split, decision)
                dc_parent[sz_split] = i_node
                ls_dot.append('%d [label=<%s<br/>impurity %s<br/>samples %s>, fillcolor="#e5813900"] ;' % (i_node,
                                                                                                           decision.replace(
                                                                                                               '>=',
                                                                                                               '&ge;').replace(
                                                                                                               '?', ''),
                                                                                                           sz_impurity,
                                                                                                           sz_samples))
            else:
                ls_dot.append('%d [label=<impurity %s<br/>samples %s<br/>class %s>, fillcolor="#e5813900"] ;' % (i_node,
                                                                                                                 sz_impurity,
                                                                                                                 sz_samples,
                                                                                                                 decision))

            if sz_parent != 'null':
                if b_branch:
                    sz_angle = '45'
                    sz_head_label = 'True'
                else:
                    sz_angle = '-45'
                    sz_head_label = 'False'
                sz_split = '%d-%s' % (nSplit, sz_parent)
                p_node = dc_parent[sz_split]
                if nSplit == 1:
                    ls_dot.append('%d -> %d [labeldistance=2.5, labelangle=%s, headlabel="%s"] ;' % (p_node,
                                                                                                     i_node, sz_angle,
                                                                                                     sz_head_label))
                else:
                    ls_dot.append('%d -> %d ;' % (p_node, i_node))
            i_node += 1
    ls_dot.append('}')
    _dot_data = '\n'.join(ls_dot)
    return _dot_data


def load_csv(file):
    """Loads a CSV file and converts all floats and ints into basic datatype."""

    def convert_types(s):
        s = s.strip()
        try:
            return float(s) if '.' in s else int(s)
        except ValueError:
            return s

    reader = csv.reader(open(file, 'rt'))
    dc_header = {}
    if bHeader:
        ls_header = next(reader)
        for i, szY in enumerate(ls_header):
            sz_col = 'Column %d' % i
            dc_header[sz_col] = str(szY)
    return dc_header, [[convert_types(item) for item in row] for row in reader]


if __name__ == '__main__':

    # Select the example you want to classify
    example = 4
    '''
    example 1. tbc
            2. iris
            3. ellipse100
            4. cross200    
    '''
    # All examples do the following steps:
    #   1. Load training data
    #   2. Let the decision tree grow
    #   4. Plot the decision tree
    #   5. classify without missing data
    #   6. Classify with missing data
    #   (7.) Prune the decision tree according to a minimal gain level
    #   (8.) Plot the pruned tree

    if example == 1:
        # the smaller examples
        bHeader = False
        dcHeadings, trainingData = load_csv('./Dataset/tbc.csv')
        decisionTree = grow_decision_tree_from(trainingData)
        # decisionTree = growDecisionTreeFrom(trainingData, evaluationFunction=gini) # with gini
        plot(decisionTree)
        # print(result)
        dot_data = dotgraph(decisionTree)
        graph = pydotplus.graph_from_dot_data(dot_data)
        graph.write_pdf("./OutputTree/tbc.pdf")
        graph.write_png("./OutputTree/tbc.png")

        print(classify(['ohne', 'leicht', 'Streifen', 'normal', 'normal'], decisionTree, data_missing=False))
        print(
            classify([None, 'leicht', None, 'Flocken', 'fiepend'], decisionTree, data_missing=True))  # no longer unique

        # Don' forget if you compare the resulting tree with the tree in my presentation: here it is a binary tree!

    elif example == 2:
        bHeader = True
        # the bigger example
        dcHeadings, trainingData = load_csv('./Dataset/fishiris.csv')  # demo data from matlab
        decisionTree = grow_decision_tree_from(trainingData, evaluation_function=gini)
        prune(decisionTree, 0, notify=True)  # notify, when a branch is pruned (one time in this example)
        plot(decisionTree)
        dot_data = dotgraph(decisionTree)
        graph = pydotplus.graph_from_dot_data(dot_data)
        graph.write_pdf("./OutputTree/iris.pdf")
        graph.write_png("./OutputTree/iris.png")

        print(classify([6.0, 2.2, 5.0, 1.5], decisionTree))  # dataMissing=False is the default setting
        print(classify([None, None, None, 1.5], decisionTree, data_missing=True))  # no longer unique

    elif example == 3:
        bHeader = True
        dcHeadings, trainingData = load_csv('./Dataset/ellipse100.csv')
        decisionTree = grow_decision_tree_from(trainingData, evaluation_function=gini)
        prune(decisionTree, 0, notify=True)  # Don't prune any branch
        plot(decisionTree)
        dot_data = dotgraph(decisionTree)
        graph = pydotplus.graph_from_dot_data(dot_data)
        graph.write_png("./OutputTree/ellipse100.png")

    elif example == 4:
        bHeader = True
        # the bigger example
        dcHeadings, trainingData = load_csv('./Dataset/cross200.csv')
        print(dcHeadings)
        decisionTree = grow_decision_tree_from(trainingData, evaluation_function=gini)
        prune(decisionTree, 0, notify=True)  # Don't prune any branch
        plot(decisionTree)
        dot_data = dotgraph(decisionTree)
        graph = pydotplus.graph_from_dot_data(dot_data)
        graph.write_png("./OutputTree/cross200.png")
