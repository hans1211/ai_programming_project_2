from collections import defaultdict

import pydotplus

import CSVReader
import random
from math import log, sqrt


class MyDecisionTreeClassifier:
    class DecisionNode:
        def __init__(self, col=-1, value=None, results=None, tb=None, fb=None, dc_headings=None, summary=None):
            self.col = col
            self.value = value
            self.results = results  # None for nodes, not None for leaves
            self.tb = tb
            self.fb = fb
            self.dc_headings = dc_headings
            self.summary = summary

        def plot(self):
            """Plots the obtained decision tree. """

            def to_string(dc_tree, dc_headings, indent=''):
                if dc_tree.results is not None:  # leaf node
                    ls_x = [(x, y) for x, y in dc_tree.results.items()]
                    ls_x.sort()
                    sz_y = ', '.join(['%s: %s' % (x, y) for x, y in ls_x])
                    return sz_y
                else:
                    sz_col = 'Column %s' % dc_tree.col
                    if sz_col in dc_headings:
                        sz_col = dc_headings[sz_col]
                    if isinstance(dc_tree.value, int) or isinstance(dc_tree.value, float):
                        decision = '%s >= %s?' % (sz_col, dc_tree.value)
                    else:
                        decision = '%s == %s?' % (sz_col, dc_tree.value)
                    true_branch = indent + 'yes -> ' + to_string(dc_tree.tb, indent + '\t\t')
                    false_branch = indent + 'no  -> ' + to_string(dc_tree.fb, indent + '\t\t')
                    return decision + '\n' + true_branch + '\n' + false_branch

            print(to_string(self, dc_headings=self.dc_headings))

        def dotgraph(self):
            dc_nodes = defaultdict(list)
            """Plots the obtained decision tree. """

            def to_string(_i_split, _decision_tree, _b_branch, _sz_parent="null", indent=""):
                if _decision_tree.results is not None:  # leaf node
                    ls_x = [(x, y) for x, y in _decision_tree.results.items()]
                    ls_x.sort()
                    sz_y = ', '.join(['%s: %s' % (x, y) for x, y in ls_x])
                    dc_y = {"name": sz_y, "parent": _sz_parent}
                    dc_summary = _decision_tree.summary
                    dc_nodes[_i_split].append(['leaf', dc_y['name'], _sz_parent, _b_branch, dc_summary['impurity'],
                                               dc_summary['samples']])
                    return dc_y
                else:
                    sz_col = 'Column %s' % _decision_tree.col
                    if sz_col in self.dc_headings:
                        sz_col = self.dc_headings[sz_col]
                    if isinstance(_decision_tree.value, int) or isinstance(_decision_tree.value, float):
                        _decision = '%s >= %s' % (sz_col, _decision_tree.value)
                    else:
                        _decision = '%s == %s' % (sz_col, _decision_tree.value)
                    trueBranch = to_string(_i_split + 1, _decision_tree.tb, True, _decision, indent + '\t\t')
                    falseBranch = to_string(_i_split + 1, _decision_tree.fb, False, _decision,
                                            indent + '\t\t')
                    dc_summary = _decision_tree.summary
                    dc_nodes[_i_split].append([_i_split + 1, _decision, _sz_parent, _b_branch, dc_summary['impurity'],
                                               dc_summary['samples']])
                    return

            to_string(0, self, None)
            ls_dot = ['digraph Tree {',
                      'node [shape=box, style="filled, rounded", color="black", fontname=helvetica] ;',
                      'edge [fontname=helvetica] ;'
                      ]
            i_node = 0
            dc_parent = {}
            for nSplit in range(len(dc_nodes)):
                ls_y = dc_nodes[nSplit]
                for lsX in ls_y:
                    i_split, decision, sz_parent, b_branch, sz_impurity, sz_samples = lsX
                    if type(i_split) == int:
                        sz_split = '%d-%s' % (i_split, decision)
                        dc_parent[sz_split] = i_node
                        ls_dot.append(
                            '%d [label=<%s<br/>impurity %s<br/>samples %s>, fillcolor="#e5813900"] ;' % (i_node,
                                                                                                         decision.replace(
                                                                                                             '>=',
                                                                                                             '&ge;').replace(
                                                                                                             '?', ''),
                                                                                                         sz_impurity,
                                                                                                         sz_samples))
                    else:
                        ls_dot.append(
                            '%d [label=<impurity %s<br/>samples %s<br/>class %s>, fillcolor="#e5813900"] ;' % (i_node,
                                                                                                               sz_impurity,
                                                                                                               sz_samples,
                                                                                                               decision))

                    if sz_parent != 'null':
                        if b_branch:
                            sz_angle = '45'
                            sz_head_label = 'True'
                        else:
                            sz_angle = '-45'
                            sz_head_label = 'False'
                        sz_split = '%d-%s' % (nSplit, sz_parent)
                        p_node = dc_parent[sz_split]
                        if nSplit == 1:
                            ls_dot.append('%d -> %d [labeldistance=2.5, labelangle=%s, headlabel="%s"] ;' % (p_node,
                                                                                                             i_node,
                                                                                                             sz_angle,
                                                                                                             sz_head_label))
                        else:
                            ls_dot.append('%d -> %d ;' % (p_node, i_node))
                    i_node += 1
            ls_dot.append('}')
            _dot_data = '\n'.join(ls_dot)
            return _dot_data

    """
    :param  max_depth:          Maximum number of splits during training
    :param  random_features:    If False, all the features will be used to
                                train and predict. Otherwise, a random set of
                                size sqrt(nb features) will be chosen in the
                                features.
                                Usually, this option is used in a random
                                forest.
    """

    def __init__(self, max_depth=-1, random_features=True, dc_header=None):
        self.root_node = None
        self.max_depth = max_depth
        self.features_indexes = []
        self.random_features = random_features
        self.dc_header = dc_header

    """
    :param  rows:       The data used to rain the decision tree. It must be a
                        list of lists. The last value of each inner list is the
                        value to predict.
    :param  criterion:  The function used to split data at each node of the
                        tree. If None, the criterion used is entropy.
    """

    def fit(self, rows, criterion=None):
        if len(rows) < 1:
            raise ValueError("Not enough samples in the given dataset")

        if criterion == "entropy":
            criterion = self.entropy
        elif criterion == "gini":
            criterion = self.gini
        else:
            criterion = self.entropy
        if self.random_features:
            self.features_indexes = self.choose_random_features(rows[0])
            rows = [self.get_features_subset(row) + [row[-1]] for row in rows]

        self.root_node = self.build_tree(rows, criterion, self.max_depth)

    """
    Returns a prediction for the given features.
    :param  features:   A list of values
    """

    def predict(self, features):
        if self.random_features:
            if not all(i in range(len(features))
                       for i in self.features_indexes):
                raise ValueError("The given features don't match\
                                 the training set")
            features = self.get_features_subset(features)

        return self.classify(features, self.root_node)

    """
    Randomly selects indexes in the given list.
    If the dataset has n features, choose sqrt(n) features randomly.
    """

    @staticmethod
    def choose_random_features(row):
        nb_features = len(row) - 1
        return random.sample(range(nb_features), int(sqrt(nb_features)))

    """
    Returns the randomly selected values in the given features
    """

    def get_features_subset(self, row):
        return [row[i] for i in self.features_indexes]

    """
    Divides the given dataset depending on the value at the given column index.
    :param  rows:   The dataset
    :param  column: The index of the column used to split data
    :param  value:  The value used for the split
    """

    @staticmethod
    def divide_set(rows, column, value):
        if isinstance(value, int) or isinstance(value, float):
            split_function = lambda row: row[column] >= value
        else:
            split_function = lambda row: row[column] == value

        set1 = [row for row in rows if split_function(row)]
        set2 = [row for row in rows if not split_function(row)]

        return set1, set2

    """
    Returns the occurrence of each result in the given dataset.
    :param  rows:   A list of lists with the output at the last index of
                    each one
    """

    @staticmethod
    def unique_counts(rows):
        results = {}
        for row in rows:
            # response variable is in the last column
            r = row[-1]
            if r not in results:
                results[r] = 0
            results[r] += 1
        return results

    """
    Returns the entropy in the given rows.
    :param  rows:   A list of lists with the output at the last index of
                    each one
    """

    def entropy(self, rows):
        log2 = lambda x: log(x) / log(2)
        results = self.unique_counts(rows)

        ent = 0.0
        for r in results.keys():
            p = float(results[r]) / len(rows)
            ent = ent - p * log2(p)
        return ent

    """
    Returns the Gini's impurity in the given rows.
    :param  rows:   A list of lists with the output at the last index of
                    each one
    """

    def gini(self, rows):
        total = len(rows)
        counts = self.unique_counts(rows)
        impurity = 0.0

        for k1 in counts:
            p1 = float(counts[k1]) / total
            for k2 in counts:
                if k1 == k2:
                    continue
                p2 = float(counts[k2]) / total
                impurity += p1 * p2
        return impurity

    """
    Recursively creates the decision tree by splitting the dataset until no
    gain of information is added, or until the max depth is reached.
    :param  rows:   The dataset
    :param  func:   The function used to calculate the best split and stop
                    condition
    :param  depth:  The current depth in the tree
    """

    def build_tree(self, rows, func, depth):
        if len(rows) == 0:
            return self.DecisionNode(dc_headings=self.dc_header)
        if depth == 0:
            return self.DecisionNode(results=self.unique_counts(rows), dc_headings=self.dc_header)

        current_score = func(rows)
        best_gain = 0.0
        best_criteria = None
        best_sets = None
        column_count = len(rows[0]) - 1

        for col in range(0, column_count):
            column_values = {}
            for row in rows:
                column_values[row[col]] = 1
            for value in column_values.keys():
                set1, set2 = self.divide_set(rows, col, value)

                p = float(len(set1)) / len(rows)
                gain = current_score - p * func(set1) - (1 - p) * func(set2)
                if gain > best_gain and len(set1) > 0 and len(set2) > 0:
                    best_gain = gain
                    best_criteria = (col, value)
                    best_sets = (set1, set2)

        _summary = {'impurity': '%.3f' % current_score, 'samples': '%d' % len(rows)}
        if best_gain > 0:
            true_branch = self.build_tree(best_sets[0], func, depth - 1)
            false_branch = self.build_tree(best_sets[1], func, depth - 1)
            return self.DecisionNode(col=best_criteria[0],
                                     value=best_criteria[1],
                                     tb=true_branch, fb=false_branch,
                                     dc_headings=self.dc_header,
                                     summary=_summary)
        else:
            return self.DecisionNode(results=self.unique_counts(rows),
                                     dc_headings=self.dc_header,
                                     summary=_summary)

    """
    Makes a prediction using the given features.
    :param  observation:    The features to use to predict
    :param  tree:           The current node
    """

    def classify(self, observation, tree):
        if tree.results is not None:
            return list(tree.results.keys())[0]
        else:
            v = observation[tree.col]
            if isinstance(v, int) or isinstance(v, float):
                if v >= tree.value:
                    branch = tree.tb
                else:
                    branch = tree.fb
            else:
                if v == tree.value:
                    branch = tree.tb
                else:
                    branch = tree.fb
            return self.classify(observation, branch)

    def plot(self):
        self.root_node.plot()

    def dotgraph(self):
        return self.root_node.dotgraph()


def test_tree():
    headers, data = CSVReader.read_iris()
    tree = MyDecisionTreeClassifier(random_features=True, dc_header=headers)
    tree.fit(data, criterion="gini")
    dot_data = tree.dotgraph()
    graph = pydotplus.graph_from_dot_data(dot_data)
    graph.write_png("./OutputTree/iris.png")


if __name__ == '__main__':
    test_tree()
