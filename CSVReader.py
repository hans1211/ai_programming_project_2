import csv


def convert_types(s):
    s = s.strip()
    try:
        return float(s) if '.' in s else int(s)
    except ValueError:
        return s


def load_csv(file, header_col=True):
    """Loads a CSV file and converts all floats and ints into basic datatype."""
    reader = csv.reader(open(file, 'rt'))
    dc_header = {}
    if header_col:
        ls_header = next(reader)
        for i, szY in enumerate(ls_header):
            sz_col = 'Column %d' % i
            dc_header[sz_col] = str(szY)
    return dc_header, [[convert_types(item) for item in row] for row in reader]


def read_iris():
    filepath = "./Dataset/fishiris.csv"
    return load_csv(filepath, header_col=True)


def read_cross200():
    filepath = "./Dataset/cross200.csv"
    return load_csv(filepath, header_col=False)


def read_ellipse100():
    filepath = "./Dataset/ellipse100.csv"
    return load_csv(filepath, header_col=False)


def read_income():
    filepath = "./Dataset/income.csv"
    return load_csv(filepath, header_col=False)


def read_breast_cancer_wisconsin():
    filepath = "./Dataset/breast_cancer_wisconsin.csv"
    return load_csv(filepath, header_col=False)


def read_optdigits():
    filepath = "./Dataset/optdigits.csv"
    return load_csv(filepath, header_col=False)


def read_wine():
    filepath = "./Dataset/winequality-white.csv"
    return load_csv(filepath, header_col=True)


def read_glass():
    filepath = "./Dataset/glass.csv"
    return load_csv(filepath, header_col=False)


def read_ionosphere():
    filepath = "./Dataset/ionosphere.csv"
    return load_csv(filepath, header_col=False)
